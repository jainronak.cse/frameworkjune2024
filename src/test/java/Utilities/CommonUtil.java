package Utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Base.BaseClass;

public class CommonUtil extends BaseClass {
	WebDriver driver;

	public CommonUtil(WebDriver driver) {
		this.driver = driver;
	}
	public void sendKeys(WebElement we,String text) {
		we.sendKeys(text);
	}
	public void click(WebElement we) {
		we.click();
	}
}
