package Utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ActionUtil {
	WebDriver driver;
	Actions act;
	public ActionUtil(WebDriver driver) {
		this.driver=driver;
		act = new Actions(driver);
	}
	
	public void moveToElement(WebElement we) {
		act.moveToElement(we).build().perform();
	}
}
