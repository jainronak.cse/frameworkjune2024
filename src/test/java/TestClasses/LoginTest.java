package TestClasses;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.BaseClass;
import PageObject.LoginPage;
import Utilities.ExcelUtility;

public class LoginTest extends BaseClass {

	LoginPage lp;

	@BeforeMethod
	public void initC() {
		init();
		lp = new LoginPage(driver);
	}

	@Test(description = "Valid Login")
	public void validLogin() {
		lp.login(prop.getProperty("username"),prop.getProperty("password"));
		Assert.assertEquals("Altoro Mutual", lp.verifyLogin());
	}
	@Test(description = "Invalid Login")
	public void invalidLogin() {
		lp.login("adsf","asd");
		Assert.assertEquals("Altoro Mutual", lp.verifyLogin());
	}
	@Test
	public void readData() {
		ExcelUtility oj = new ExcelUtility();
		Object o = oj.getCellData(0, 0);
		System.out.println(o.toString());
	}
	

}
