package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Base.BaseClass;

public class LoginPage extends BaseClass {

	@FindBy(id = "uid")
	WebElement userName;
	@FindBy(id = "passw")
	WebElement password;
	@FindBy(name = "btnSubmit")
	WebElement btnLogin;

	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public void login(String user, String pwd) {
		commonUtil.sendKeys(userName, user);
		commonUtil.sendKeys(password, user);
		btnLogin.click();
	}

	public String verifyLogin() {
		return driver.getTitle();
	}
}
