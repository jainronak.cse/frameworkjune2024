package Base;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import Utilities.CommonUtil;
import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseClass {
	public static Properties prop;
	public static WebDriver driver;
	public static CommonUtil commonUtil;

	public BaseClass() {
		try {
			File f = new File(".//config.properties");
			FileInputStream fis = new FileInputStream(f);
			prop = new Properties();
			prop.load(fis);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public WebDriver getDriver() {
		return BaseClass.driver;
	}

	public void init() {
		WebDriverManager.chromedriver().setup();
		String bName = prop.getProperty("browser");
		if (bName.equalsIgnoreCase("chrome")) {
			driver = new ChromeDriver();
		}
		driver.manage().window().maximize();
		driver.get(prop.getProperty("url"));
		commonUtil = new CommonUtil(driver);
	}

	public void tearDown() {
		driver.quit();
	}

}
